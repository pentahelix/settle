#version 130

uniform sampler2D tex0;
uniform int team;

in vec2 tcoord;
out vec4 glColor;

void main(){
  vec2 uv = vec2(ceil(tcoord.x * 960.0f) / 960.0f, ceil(tcoord.y * 640.0f) / 640.0f);
  glColor = texture2D(tex0, uv);
}
