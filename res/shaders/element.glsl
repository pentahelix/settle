#version 130

uniform sampler2D tex0;
uniform int team;

const vec4 teamColor[6] = vec4[](vec4(.675, .2, .2, 1), vec4(.36, .431, .88, 1), vec4(1,1,1,1), vec4(1,1,1,1), vec4(1,1,1,1), vec4(1,1,1,1));

in vec2 tcoord;

out vec4 glColor;

void main(){
  vec4 c = texture2D(tex0, tcoord);
  if(c.r == 1 && c.b == 1 && c.g == 0){
    glColor = teamColor[team];
  }else{
    glColor = c;
  }
  return;
}
