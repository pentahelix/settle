#version 130

uniform sampler2D tex0;
uniform int _connections;
uniform vec4 _color0;
uniform vec4 _color1;

in vec2 tcoord;

out vec4 glColor;

void main(){
  vec4 c = texture2D(tex0, tcoord);
  if(c.g == 1 && c.b == 1 && c.r != 1){
    if((int(c.r*255/4) & _connections) != (int(c.r*255/4))){
      glColor = _color0;
    }else{
      glColor = _color1;
    }
  }else{
    glColor = c;
  }
  return;
}
