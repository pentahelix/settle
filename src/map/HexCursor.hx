package map;

import luxe.Sprite;
import luxe.Vector;
import luxe.Input;
import luxe.Rectangle;

class HexCursor extends Sprite{
  override public function new(){
    super({
      size: new Vector(32,32),
      pos: HexMap.hex_to_pos(new Vec(0,0,0)),
      depth: 10,
    });
    disable();
  }

  override public function onmousemove(e:MouseEvent){
    pos = HexMap.hex_to_pos(HexMap.pos_to_hex(Luxe.renderer.camera.screen_point_to_world(e.pos)));
  }

  public function enable(?id:String='res/sprites/ui/hex_cursor.png'){
    texture = Luxe.resources.texture(id);
    size = new Vector(texture.width, texture.height);
    uv = new Rectangle(0,0,texture.width, texture.height);
    active = true;
    visible = true;
    var path:Array<String> = id.split("/");
    var bName:String = path[path.length-1].split(".")[0];
    if(C.buildingData[bName] != null){
      if(C.buildingData[bName].contextual){
        uv = new Rectangle(0,0,32,32);
        size = new Vector(32,32);
      }
    }
  }

  public function disable(){
    active = false;
    visible = false;
  }

  public function set_clickable(b:Bool){
    if(b){
      color = new luxe.Color().rgb(0xffffff);
    }else{
      color = new luxe.Color().rgb(0x999999);
    }
  }
}
