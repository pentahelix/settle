package map;
import UI.*;
import sys.Player;
import sys.MapGen;
import map.HexTile;
import luxe.Sprite;
import luxe.Vector;
import luxe.tween.Actuate;
import luxe.tween.MotionPath;

using Extensions.VectorExt;
using Extensions.StringExt;
using Extensions.ArrayExt;

typedef PathNode = {
  var pos:Vec;
  var previous:PathNode;
}

class HexMap{
  public static var chunks:HexChunks;


  public static var cubeDir:Array<Vec> = [
    new Vec(1,  0, -1), new Vec(1, -1,  0), new Vec(0, -1, 1),
    new Vec(-1,  0, 1), new Vec(-1, 1,  0), new Vec(0, 1, -1),
  ];

  public function new(size:Int){
    Manager.map = this;
    MapGen.generate(size, Luxe.utils.random.int(10000));
    chunks = new HexChunks();
    chunks.load_chunks(size);
    chunks.enable_necessary_chunks();
    set_connections();
  }

  public function get_tile_at(v:Vector):HexTile{
    var k:String = v.to_string();
    return chunks._map[k];
  }

  public function get_tile_at_pos(p:Vector):HexTile{
    var k:String = pos_to_hex(p).to_string();
    return chunks._map[k];
  }

  public function place_building(name:String, pos:Vector, owner:Player){
    var b:Building = new Building(name);
    b.set_hex_pos(pos);
    b.owner = owner;
    owner.buildings.push(b);
    get_tile_at(pos).buildable = false;
    if(C.buildingData[name].large){
      for(tile in adjacent_to(get_tile_at(pos))){
        tile.buildable = false;
      }
    }
  }

  public function place_unit(name:String, pos:Vector, owner:Player){
    var u:Unit = new Unit(name);
    u.set_hex_pos(pos);
    u.owner = owner;
    owner.units.push(u);
  }

  public function move_unit(u:Unit, p:Array<Vec>){
    while(get_tile_at(p[p.length-1]).unit!=null)p.pop();
    if(p.length == 0)return;
    var motion = new MotionPath();
    for(i in 1...p.length){
      var line:Vec = hex_to_pos(p[i]);
      motion.line(line.x,line.y);
      motion.line(line.x, line.y, .25);
    }

    Actuate.motionPath(u.pos, p.length/3.5, {x: motion.x, y: motion.y}).ease(luxe.tween.easing.Linear.easeNone);
    u.set_hex(p[p.length-1]);
  }

  public function nearest_unit(v:Vec, names:Array<String>){
    var dist:Int = 9999;
    var u:Unit = null;
    for(unit in Manager.player.units){
      if(names.indexOf(unit.tag) == -1)continue;
      if(HexMap.distance(unit.gridPos, v) < dist){
        dist = HexMap.distance(unit.gridPos, v);
        u = unit;
      }
    }

    return u;
  }

  public function squad_spawn():luxe.Vec{
    var dir:Int = Std.int(Math.random()*6);
    var pos:Vec = Manager.player.town_center;
    var next:Bool = true;
    var overflow:Int = 0;
    while(next){
      next = false;
      for(i in dir-1...dir+2){
        if(Manager.map.get_tile_at(pos+HexMap.cubeDir[Std.int(Math.abs(i%6))]).walkable){
          next = true;
          pos += HexMap.cubeDir[Std.int(Math.abs(i%6))];
        }
      }
    }
    return pos;
  }

  public static function distance(h1:Vec, h2:Vec){
    return cast((Math.abs(h1.x - h2.x) + Math.abs(h1.y - h2.y) + Math.abs(h1.z - h2.z)) / 2, Int);
  }

  public static function adjacent(c:Vec):Array<Vec>{
    var result:Array<Vec> = [];
    for(v in cubeDir){
      result.push(c+v);
    }
    return result;
  }

  public static function range(c:Vec, r:Int, ?add_center:Bool=false){
    var res:Array<Vec> = [];
    for(dx in -r...r+1){
      for(dy in Std.int(Math.max(-r, -dx-r))...Std.int(Math.min(r, -dx+r))+1){
        var dz = -dx-dy;
        if(dx == 0 && dy == 0)continue;
        res.push(c+new Vec(dx,dy,dz));
      }
    }
    if(add_center)res.push(c);
    return res;
  }

  public static function ring(c:Vec, r:Int){
    var res:Array<Vec> = [];
    var v:Vec = c + cubeDir[4]*r;
    for(i in 0...6){
      for(j in 0...r){
        res.push(v);
        v = v+cubeDir[i];
      }
    }
    return res;
  }

  public function adjacent_to(tile:HexTile):Array<HexTile>{
    var result:Array<HexTile> = [];
    for(v in cubeDir){
      result.push(get_tile_at(tile.gridPos+v));
    }
    return result;
  }

  public function nearest(h:Vec, ids:Array<Int>):HexTile{
    if(ids.indexOf(get_tile_at(h).tileId)!=-1)return get_tile_at(h);
    var range:Int = 1;
    var overflow:Bool = false;
    while(true){
      for(tile in ring(h, range)){
        if(ids.indexOf(get_tile_at(tile).tileId)!=-1)return get_tile_at(tile);
      }
      range++;
      if(range>1000)return null;
    }
  }

  public function nearest_tile(h:Vec, name:String){
    if(get_tile_at(h).data.name==name)return get_tile_at(h);
    var range:Int = 1;
    var overflow:Bool = false;
    while(true){
      for(tile in ring(h, range)){
        if(get_tile_at(tile).data.name==name)return get_tile_at(tile);
      }
      range++;
      if(range>1000)return null;
    }
  }

  public function find_path(p1:Vec, p2:Vec, ?max_len:Int=9999):Array<Vec>{
    if(get_tile_at(p1) == null || get_tile_at(p2) == null)return null;
    if(!get_tile_at(p1).data.passable || !get_tile_at(p2).data.passable)return null;
    if(p1.equals(p2))return null;

    var queued:Array<String> = [];

    var frontier:Array<PathNode> = [];
    frontier.push({
      pos: p1,
      previous: null,
    });

    var visited:Array<String> = [];

    var found:Bool = false;
    var lastNode:PathNode = null;
    while(!found){
      var node = frontier[0];
      if(node == null || node.pos == null){
        return null;
      }
      visited.push(node.pos.to_string());
      frontier.remove(node);

      for(neighbour in adjacent(node.pos)){
        var tileAt:HexTile = get_tile_at(neighbour);
        if(tileAt == null)continue;
        if(visited.indexOf(neighbour.to_string()) == -1 && tileAt.data.passable && queued.indexOf(neighbour.to_string()) == -1){
          if(tileAt.unit == null){
            var newNode:PathNode = {
              pos: neighbour,
              previous: node
            };
            if(neighbour.equals(p2)){
              found = true;
              lastNode = newNode;
            }
            frontier.push(newNode);
            queued.push(neighbour.to_string());
          }else if(neighbour.equals(p2)){
            found = true;
            lastNode = node;
          }
        }
      }
    }

    var next:PathNode = lastNode;
    var path:Array<Vec> = [];
    while(next != null){
      path.unshift(next.pos);
      next = next.previous;
    }

    while(path.length-1 > max_len){
      path.pop();
    }
    return path;
  }


  private function set_connections():Void{
    for(tile in chunks._map){
      var i:Int = 1;
      for(t in adjacent_to(tile)){
        if(t!=null){
          if(t.tileId==tile.tileId){
            tile.borders |= i;
          }
        }
        i*=2;
      }
    }
  }

  public function set_connections_of(t:HexTile){
    var tiles = [t].concat(adjacent_to(t));
    for(tile in tiles){
      if(tile == null)continue;
      tile.borders = 0;
      var i:Int = 1;
      for(t in adjacent_to(tile)){
        if(t!=null){
          if(t.tileId==tile.tileId||t.data.permit.indexOf(tile.tileId)!=-1){
            tile.borders |= i;
          }
        }
        i*=2;
      }
    }
  }

  //UTILITY
  public static function hex_to_pos(pos:Vector):Vec{
    return new Vec(32*(pos.x+pos.z/2), 25 * pos.z);
  }

  public static function pos_to_hex(hex:Vector):Vec{
    var q:Float = (25*hex.x-16*hex.y)/800;
    var r:Float = 32*hex.y/800;
    return new Vec(q,-q-r,r).nearest_hex();
  }

  private var display:Array<UI.HexHighlight> = [];
  private var displayArr:Array<Vec> = [new Vec(1,1,1), new Vec(1,1,1)];

  public function display_array(disp:Array<Vec>, ?anim:Bool=true){
    display_clear();
    if(disp==null)return;
    if(displayArr[0].equals(disp[0]) && displayArr[1].equals(disp[disp.length-1]))return;
    var i:Int = 0;
    for(v in disp){
      display.push(new UI.HexHighlight(v, i*(.15/disp.length), anim));
      i++;
    }
    displayArr[0] = disp[0];
    displayArr[1] = disp[disp.length-1];
  }

  public function display_contains(ve:Vec){
    for(v in display){
      if(v.gridPos.equals(ve))return true;
    }
    return false;
  }

  public function display_clear(){
    for(sprite in display)sprite.destroy(true);
    display = [];
    displayArr = [new Vec(1,1,1), new Vec(1,1,1)];
  }

  public function can_build(name:String, h:Vec){
    if(get_tile_at(h).buildable == false || !C.buildingData[name].large)return get_tile_at(h).buildable;
    for(tile in adjacent_to(get_tile_at(h))){
      if(tile.buildable == false)return false;
    }
    return true;
  }
}
