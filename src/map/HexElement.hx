package map;

import luxe.Sprite;
import luxe.Vector;
import phoenix.Shader;

class HexElement extends Sprite{
  public var gridPos:Vec;
  public var tag:String;
  public var tile:HexTile;
  public var readiness(default, set):Int;
  public var range:Int;
  public var interactable:Bool = false;
  public var owner(default, set):sys.Player;
  public var health(default, set):Int = 10;
  public static var shaders:Array<Shader> = [];

  override public function new(t:String, s:String, ?inter:Bool=false){
    super({
      depth: 2,
    });
    tag = t;
    texture = Luxe.resources.texture('res/sprites/${s}');
    size = new Vector(texture.width, texture.height);
    set_hex_pos(new Vec(0,0,0));

    interactable = inter;
  }

  public function set_hex(v:Vec){
    gridPos = v;
    if(tile != null)tile.elements.remove(this);
    tile = Manager.map.get_tile_at(v);
    tile.elements.push(this);
  }

  public function set_hex_pos(v:Vec){
    pos = HexMap.hex_to_pos(v);
    set_hex(v);
  }

  public function set_readiness(v:Int){
    readiness = v;
    color = (v == 0) ?  new luxe.Color().rgb(0x777777) : new luxe.Color().rgb(0xffffff);
    return v;
  }

  public function set_owner(o:sys.Player){
    owner = o;
    if(shaders[owner.num] == null){
      shader = Luxe.resources.shader('element').clone('element_${owner.num}');
      shader.set_int('team', owner.num);
      shaders[owner.num] = shader;
    }else{
      shader = shaders[owner.num];
    }
    return o;
  }

  public function attack(u:Unit){
    u.health -= 1;
    readiness = 0;

  }

  public function kill(){

  }

  public function set_health(v:Int){
    health = v;
    if(health <= 0)kill();
    return v;
  }
}
