package map;

import luxe.Sprite;
import luxe.Vector;
import luxe.Entity;
import luxe.tween.Actuate;
import luxe.tween.MotionPath;
import sys.FX;

typedef UnitData = {
  var speed:Int;
  var sprite:String;
  var damage:Int;
  var health:Int;
}

class Unit extends HexElement{
  public var data:UnitData;
  public var max_health:Int;
  public var gathering(default, set):Bool = false;
  public var gatheringName:String = "";
  public var hIndicator:HealthIndicator;

  override public function new(name:String){
    super(name,C.unitData[name].sprite);
    depth = 5;
    data = C.unitData[tag];
    readiness = 2;
    hIndicator = new HealthIndicator(this);
    max_health = health = data.health;
    range = data.speed;
    Luxe.events.listen("end_turn", end_turn);
  }

  override public function kill(){
    owner.units.remove(this);
    tile.elements.remove(this);
    FX.death(gridPos);
    destroy();
  }

  override public function attack(u:Unit){
    if(['soldier', 'knight'].indexOf(tag) !=-1){
      FX.slash(u .gridPos);
      var motion = new MotionPath();
      var target:Vec = Vector.Subtract(u.pos, pos).divideScalar(3).add(pos);
      motion.line(target.x,target.y);
      motion.line(pos.x,pos.y);
      Actuate.motionPath(pos, .5, {x: motion.x, y: motion.y}).ease(luxe.tween.easing.Cubic.easeOut);
    }
    if(['archer', 'marksman'].indexOf(tag) !=-1)FX.arrow(gridPos, u.gridPos);
    u.health -= data.damage;
    readiness = 0;
  }

  override public function set_health(v:Int){
    super.set_health(v);
    hIndicator.set_health(v);
    return v;
  }

  public function end_turn(_){
    if(gathering){
      if(readiness >= 2){
        if(tile.data.name != gatheringName){
          var target:HexTile = Manager.map.nearest_tile(gridPos, gatheringName);
          if(target == null){
            gathering = false;
            return;
          }
          if(map.HexMap.distance(gridPos, target.gridPos) > 6){
            gathering = false;
            return;
          }
          var path:Array<Vec> = Manager.map.find_path(gridPos, target.gridPos, data.speed);
          Manager.map.move_unit(this, path);
        }
      }

      if(readiness >= 1 && tile.data.name == gatheringName){
        tile.gather();
      }
    }
  }

  public function set_gathering(v:Bool){
    gathering = v;
    gatheringName = tile.data.name;
    return v;
  }
}


class HealthIndicator extends Entity{
  var health:Array<Sprite> = [];
  var armor:Array<Sprite> = [];
  override public function new(p:Unit){
    super({
      parent: p,
      pos: new Vec(8,8),
    });
    for(i in 0...p.data.health){
      add_health();
    }

    for(i in 0...4){
      add_armor();
    }
  }

  public function set_health(v:Int){
    for(h in health){
      if(health.indexOf(h) < v)h.visible = true;
      else h.visible = false;
    }
  }

  public function add_health(){
    var i:Int = health.length;
    health.push(new Sprite({
      parent: this,
      pos: new Vec(0, 3*i),
      texture: Luxe.resources.texture('res/sprites/ui/health_icon.png'),
      centered: false,
      depth: 5,
    }));
  }

  public function add_armor(){
    var i:Int = armor.length;
    armor.push(new Sprite({
      parent: this,
      depth: 5,
      pos: new Vec(14, 3*i),
      texture: Luxe.resources.texture('res/sprites/ui/armor_icon.png'),
      centered: false,
    }));
  }
}
