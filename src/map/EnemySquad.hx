package map;

import luxe.Vector;
import UI.InfoText;
import map.*;

class EnemySquad{
  public var size:Int = 0;
  public static function spawn(difficulty:Int){
    var pos:Vec = Manager.map.squad_spawn();
    new Enemy(pos, 'soldier');
  }

  public static var enemies:Array<Enemy> = [];
  public static function control_enemies(_){
    for(enemy in enemies){
      var target = Manager.map.nearest_unit(enemy.gridPos, ['settler']);
      if(target == null)return;
      if(enemy.in_range(target)){
        enemy.attack(target);
      }else{
        // new InfoText("T", HexMap.hex_to_pos(enemy.gridPos));
        var path:Array<Vec> = Manager.map.find_path(enemy.gridPos, target.gridPos, enemy.data.speed+1);
        if(path == null){
          trace("No path found");
          return;
        }
        Manager.map.move_unit(enemy, path);
      }
    }
  }
}

class Enemy extends Unit{
  override public function new(v:Vec, n:String){
    super(n);
    EnemySquad.enemies.push(this);
    set_hex_pos(v);
    owner = Manager.ai;
  }

  public function in_range(u:Unit){
    if(HexMap.distance(gridPos, u.gridPos) == 1)return true;
    return false;
  }
}
