package map;
import luxe.Sprite;
import luxe.Vector;
import sys.MapGen;

using Extensions.VectorExt;

class HexChunks{
  public var _map:Map<String, HexTile> = new Map<String, HexTile>();

  private var loadedChunks:Map<String, HexChunk> = new Map<String, HexChunk>();

  public function new(){}

  private function load_chunk(v:Vec){
    loadedChunks.set(v.to_string(), new HexChunk(v, this));
  }

  private var lastCenter:Vec = new Vec(-500,-500);
  public function enable_necessary_chunks(){
    var necessary:Array<Vec> = [];
    var pt:Vec = Luxe.camera.pos;

    var centerChunk:Vec = new Vec(Math.round((pt.x+Luxe.screen.w/2)/96), Math.round((pt.y+Luxe.screen.h/2)/75));
    if(centerChunk.equals(lastCenter))return;

    //necessary chunks + 1 roll-over
    //TODO: Optimize
    var necX:Int = Math.ceil(Luxe.screen.w / 3 / 96)+2;
    var necY:Int = Math.ceil(Luxe.screen.h / 3 / 75)+2;

    for(x in 0...necX){
      for(y in 0...necY){
        var tx:Int = x - Std.int(necX/2);
        var ty:Int = y - Std.int(necY/2);
        necessary.push(new Vec(tx,ty)+centerChunk);
      }
    }

    //hide useless chunks
    for(chunk in loadedChunks){
      if(necessary.indexOf(chunk.chunkPos) == -1)chunk.set_v(false);
    }

    //show necessary chunks
    for(chunk in necessary){
      if(loadedChunks[chunk.to_string()]!=null)loadedChunks[chunk.to_string()].set_v(true);
    }
    lastCenter = centerChunk;
  }

  public function load_chunks(r:Int){
    var v = Math.ceil(r/3);
    for(x in -v...v+1){
      for(y in -v...v+1){
        load_chunk(new Vec(x,y));
      }
    }
  }
}

//Chunk: 96x75px

class HexChunk extends Sprite{
  public var chunkPos:Vec;
  private var hexPos:Vec;
  private var chunks:HexChunks;
  private var _tiles:Array<HexTile> = [];
  private var v:Bool = true;
  override public function new(at:Vec, ch:HexChunks){
    super({
      pos: chunk_to_pos(at),
      visible: false,
      size: new Vec(0,0),
      centered: true,
    });
    chunkPos = at;
    hexPos = chunk_to_hex(at);
    chunks = ch;
    make_tiles();
    set_v(false);
  }

  public function set_v(b:Bool){
    if(v==b)return;
    for(t in _tiles){
      t.active = b;
      t.visible = b;
    }
    v = b;
  }

  private function make_tiles(){
    var tiles = HexMap.cubeDir.copy();
    tiles.push(new Vec(0,0,0));

    tiles.push(new Vec(-1,2,-1));
    tiles.push(new Vec(-2,1,1));

    for(tile in tiles){
      var id:Int = MapGen.at(tile+hexPos);
      if(id>=0){
        make_tile(tile, id);
      }
    }

    for(tile in _tiles){
      Manager.map.set_connections_of(tile);
    }
  }

  public function make_tile(p:Vec, id:Int){
    var tile:HexTile = new HexTile(id, this);
    tile.parent = this;
    tile.set_hex(p);
    tile.gridPos = p+hexPos;
    tile.locked = true;
    chunks._map[(p+hexPos).to_string()] = tile;
    _tiles.push(tile);
  }

  private function chunk_to_pos(c:Vec){
    return new Vec(96*c.x-(16*Math.abs(c.y%2)), 75*c.y);
  }

  private function chunk_to_hex(c:Vec){
    var x:Int = 0;
    var y:Int = 0;
    var z:Int = Std.int(3*c.y);
    if(c.y%2==0){
      x = -Std.int(z/2);
      y = -Std.int(z/2);
    }else{
      x = -Math.ceil(z/2);
      y = -Math.floor(z/2);
    }
    x += Std.int(3*c.x);
    y -= Std.int(3*c.x);
    return new Vec(x,y,z);
  }
}
