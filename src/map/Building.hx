package map;

import luxe.Sprite;
import luxe.Vector;

typedef BuildingData = {
  var cost:Array<Int>;
  var sprite:String;

  var passable:Bool;
  var flip:Bool;
  var contextual:Bool;
  var large:Bool;
};

class Building extends HexElement{
  private var data:BuildingData;

  override public function new(name:String){
    data = C.buildingData[name];
    super(name, C.buildingData[name].sprite);
    depth = 4;
    if(data.contextual)size = new Vec(32,32);
    set_hex_pos(gridPos);
  }

  private var contextId:Array<Int> = [0,1,2,4,5,8,9,10,16,17,18,20,32,34,36,40];
  override public function set_hex_pos(v:Vec){
    if(data.large && tile != null){
      for(tile in Manager.map.adjacent_to(tile)){
        tile.elements.remove(this);
      }
    }

    super.set_hex_pos(v);

    if(data.contextual){
      context_set();
    }

    if(data.large){
      for(tile in Manager.map.adjacent_to(tile)){
        tile.elements.push(this);
      }
    }
  }

  public function context_set(){
    var tiles:Array<HexTile> = Manager.map.adjacent_to(tile);
    tiles.push(tile);
    for(tl in tiles){
      var wall:HexElement = tl.elements.filter(function(e){return e.tag == 'wall';})[0];
      if(wall == null)continue;
      var res:Int = 0;
      var i = 0;
      for(t in Manager.map.adjacent_to(tl)){
        for(e in t.elements){
          if(e.tag == "wall")res += cast Math.pow(2,i);
        }
        i++;
      }
      wall.uv = new luxe.Rectangle(contextId.indexOf(res)*32, 0, 32, 32);
    }
  }
}
