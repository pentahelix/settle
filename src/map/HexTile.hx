package map;

import luxe.Sprite;
import luxe.Vector;
import luxe.Color;
import sys.Turn;

typedef TileData = {
  var name:String;
  var border:Array<luxe.Color>;
  var buildable:Bool;
  var passable:Bool;
  var resources:Array<Int>;
  var variants:Int;
  var permit:Array<Int>;
}

class HexTile extends Sprite{
  public var gridPos:Vec;
  public var tileId:Int = -1;
  public var borders(default, set):Int = 0;
  public var elements:Array<HexElement> = [];
  public var data:TileData;
  public var harvests:Int = 0;
  public var buildable:Bool;
  public var walkable:Bool;

  public var unit(get, null):Unit;

  public var chunk:HexChunks.HexChunk;

  public static var harvestId:Map<Int, Int> = [
    1=>0,
    3=>0,
    12=>0,
  ];

  public static var idCycles:Map<Int, Int> = [
    5=>6,
    6=>7,
    7=>5,
    13=>14,
    14=>15,
    15=>16,
    16=>1,
  ];

  override public function new(i:Int, c:HexChunks.HexChunk){
    super({
      size: new Vector(32,32),
      texture: Luxe.resources.texture('res/sprites/tiles.png'),
      depth: 0,
    });
    chunk = c;
    uv.w = 32;
    uv.h = 32;
    load_id(i);
    Luxe.events.listen('start_turn', start_turn);
    Luxe.events.listen('end_turn', end_turn);
  }

  public function start_turn(_){
    if(idCycles[tileId] != null){
      load_id(idCycles[tileId], false);
    }
  }

  public function end_turn(_){
    if(C.tileData[tileId].name=='Field_2'){
      Turn.add_action("food", {tiles: [this], value: 1});
    }
  }

  public function load_id(id:Int, ?connect:Bool=true, ?newVariant=true){
    data = C.tileData[id];
    buildable = data.buildable;
    walkable = data.passable;

    uv.x = 32 * id%16;
    uv.y = id < 16 ? 0 : 256;
    if(newVariant)uv.y += 32*Std.int(Math.random()*data.variants);

    setup_shader();
    if(!connect){
      tileId = id;
      return;
    }

    if(tileId != -1){
      tileId = id;
      Manager.map.set_connections_of(this);
    }else{
      tileId = id;
    }
  }

  public function set_borders(v:Int){
    borders = v;
    shader.set_int("_connections", borders);
    return v;
  }

  public function set_hex(h:Vec){
    if(h.x+h.y+h.z != 0)trace('Invalid Coordniates: ${h}');
    pos = HexMap.hex_to_pos(h);
  }

  private function setup_shader(){
    var sh:phoenix.Shader = Luxe.resources.shader("hex_tile").clone("");
    sh.set_int("_connections", borders);
    sh.set_color("_color0", data.border[0]);
    sh.set_color("_color1", data.border[1]);
    shader = sh;
  }

  public function gather(){
    var r:Array<Int> = data.resources;
    if(r!=null){
      if(r[0] != 0)Turn.add_action("wood", {tiles: [this], value: r[0]});
      if(r[1] != 0)Turn.add_action("food", {tiles: [this], value: r[1]});
      if(r[2] != 0)Turn.add_action("rock", {tiles: [this], value: r[2]});
    }
    harvests++;
    if(harvests==5){
      load_id(harvestId[tileId]);
    }
  }

  public function get_unit(){
    var u:Unit = null;
    for(e in elements){
      if(Std.is(e, Unit)){
        u = cast e;
      }
    }
    return u;
  }
}
