package;

import luxe.Sprite;
import luxe.Vector;
import luxe.Events;
import luxe.Input;
import luxe.Text;
import luxe.Text.*;
import phoenix.Batcher;
import phoenix.Texture;
import luxe.tween.Actuate;
import map.Unit;
import sys.MapGen;
import snow.api.buffers.Uint8Array;

using StringTools;

class UI extends Batcher{
  public var text:Map<String, Text> = new Map<String, Text>();
  public var elementDisplay:ElementDisplay;
  public var btnTurn:HexButton;
  public var settlerBar:SettlerBar;
  public var hexButtons:HexButtons;

  public static var actionCost:Map<String, Array<Int>> = [
    "armor_equip"=> [0,0,0,1],
    "till" => [1,0,0,0],
    "train_settler_soldier" => [0,0,1,2],
    "train_settler_archer" => [1,0,1,1],
    "quarry" => [15,0,0,0],
  ];

  public function new(){
    super(Luxe.renderer, "ui");
    Luxe.renderer.add_batch(this);
    Manager.ui = this;
    view = new phoenix.Camera();
    view.zoom = 3;
    view.pos = new Vector(-Luxe.screen.width/3,-Luxe.screen.height/3);
    layer = 2;

    new Sprite({
      pos: new Vector(0,0),
      centered: false,
      texture: Luxe.resources.texture('res/sprites/ui/resource_bar.png'),
      batcher: this,
      depth: 2,
    });

    // var texture = new Texture({
    //   id: 'map tex',
    //   width: (MapGen.map.r*2-1)*4,
    //   height: MapGen.map.r*2*3,
    // });
    //
    // var pixels:Uint8Array = new Uint8Array(texture.width*texture.height*4);
    //
    // for(node in MapGen.map.nodes){
    //   var x:Int = Std.int(4 * (node.pos.x + node.pos.z/2))-2;
    //   var y:Int = Std.int(3 * node.pos.z)-2;
    //   for(oX in 0...4){
    //     for(oY in 0...4){
    //       if((oX==0&&oY==0)||(oX==0&&oY==3)||(oX==3&&oY==0)||(oX==3&&oY==3))continue;
    //       pixels[(x+oX + (y+oY)*texture.width)*4+0] = 100;
    //       pixels[(x+oX + (y+oY)*texture.width)*4+1] = 1;
    //       pixels[(x+oX + (y+oY)*texture.width)*4+2] = 1;
    //       pixels[(x+oX + (y+oY)*texture.width)*4+3] = 1;
    //     }
    //   }
    // }
    // texture.submit(pixels);

    // new Sprite({
    //   pos: new Vector(50,50),
    //   centered: false,
    //   texture: sys.MapGen.map.texture,
    //   size: new Vec(100,100),
    //   batcher: this,
    //   depth: 50,
    //   color: new luxe.Color(.5,.5,.5,1)
    // });

    settlerBar = new SettlerBar(this);

    text['wood'] = ui_text(new Vec(60,1));
    text['food'] = ui_text(new Vec(121,1));
    text['rock'] = ui_text(new Vec(182,1));
    text['iron'] = ui_text(new Vec(243,1));


    elementDisplay = new ElementDisplay(this);

    hexButtons = new HexButtons(this);

    btnTurn = new HexButton(0, this);
    btnTurn.pos = new Vec(Luxe.screen.w/3-12, 12);
    btnTurn.buttonName = 'commit_turn';
    btnTurn.set_clickable(false);
    btnTurn.icon.texture = Luxe.resources.texture('res/sprites/ui/actions/commit_turn.png');
  }

  public function resize(){
    view.viewport = new phoenix.Rectangle(0,0,Luxe.screen.w, Luxe.screen.h);
    view.pos = new Vector(-Luxe.screen.width/3,-Luxe.screen.height/3);
    elementDisplay.pos = new Vec(Luxe.screen.w/3-45,Luxe.screen.h/3-48);
    settlerBar.pos = new Vec(0,Luxe.screen.h/3-13);
    btnTurn.pos = new Vec(Luxe.screen.w/3-12, 12);
  }

  static var txt:Array<String> = ["wood", "food", "rock", "iron"];
  public function preview_cost(cost:Array<Int>){
    for(i in 0...4){
      if(cost[i] != 0){
        text[txt[i]].color = new luxe.Color().rgb(0xac3232);
        text[txt[i]].text = Manager.player.resources[txt[i]]-cost[i]+"";
      }
    }
  }

  public function preview_cost_reset(){
    for(i in 0...4){
      text[txt[i]].color = new luxe.Color().rgb(0x8f563b);
      text[txt[i]].text = Manager.player.resources[txt[i]]+"";
    }
  }

  private function ui_text(pos:Vec, ?text:String='0', ?align:TextAlign=TextAlign.right):Text{
    return new Text({
      text: text,
      font: Luxe.resources.font('res/fonts/font.fnt'),
      point_size: 14,
      pos: pos,
      align: align,
      color: new luxe.Color().rgb(0x8f563b),
      batcher: this,
      depth: 3,
    });
  }

  public function toggle_pause_menu(){

  }
}

class ElementDisplay extends Sprite{
  private var element:Sprite;
  override public function new(b:Batcher){
    super({
      pos: new Vec(Luxe.screen.w/3-45,Luxe.screen.h/3-48),
      batcher: b,
      centered: false,
      texture: Luxe.resources.texture("res/sprites/ui/info_element.png"),
    });

    element = new Sprite({
      pos: new Vec(22,24),
      size: new Vec(32,32),
      parent: this,
      depth: 5,
      batcher: b,
    });

    set_element('');
  }

  public function set_element(id:String){
    if(id==''){
      element.visible = false;
      return;
    }
    element.shader = map.HexElement.shaders[Manager.player.num];
    element.texture = Luxe.resources.texture(id);
    element.visible = true;
  }
}

class HexButtons extends luxe.Entity{
  private var ui:UI;
  private var buttons:Array<HexButton> = [];
  private var cur:String;
  override public function new(batcher:UI){
    super({
      name: 'hex_buttons',
      pos: new Vec(0,0),
    });
    ui = batcher;
    pos = new Vec(0,0);
    for(i in 0...6){
      var btn = new HexButton(i);
      btn.set_parent(this);
      buttons.push(btn);
    }
  }

  public function show(name:String, ?cat:String='default'){
    var actions:Array<String> = C.actions[name][cat];
    for(i in 0...6){
      var btn:HexButton = buttons[i];
      if(actions.length-1 > i){
        btn.scale = new Vec(1,1);
        btn.icon.texture = Luxe.resources.texture('res/sprites/ui/actions/${actions[i]}.png');
        btn.buttonName = actions[i];
        btn.set_clickable(clickable(actions[i]));
      }else{
        btn.scale = new Vec(0,0);
      }
    }
    cur = cat;
  }

  public function clear(){
    for(btn in buttons){
      btn.scale = new Vec(0,0);
    }
  }

  public function back():Bool{
    if(Manager.selectedElement == null)return false;
    var tag:String = Manager.selectedElement.tag;
    var actions:Array<String> = C.actions[tag][cur];
    var back:String = actions[actions.length-1];
    if(back != 'exit'){
      show(tag, back);
    }else{
      clear();
    }
    return back != 'exit';
  }

  public function validate(){
    for(btn in buttons){
      btn.set_clickable(clickable(btn.buttonName));
    }
  }

  private function clickable(action:String){
    var r:Int = Manager.selectedElement.readiness;
    var act:Array<String> = action.split("_");

    if(r < 2 && ['move'].indexOf(action) != -1)return false;
    if(r < 1 && ['build', 'till', 'shoot', 'attack', 'harvest'].indexOf(action) != -1)return false;

    if(act.length > 1 && C.buildingData[act[1]] != null){
      if(act[0] == "build")return Manager.player.is_available(C.buildingData[act[1]].cost);
    }
    return true;
  }
}

class HexButton extends Sprite{
  public var index:Int;
  public var icon:Sprite;
  public var buttonName:String;
  private var clickable:Bool = true;
  override public function new(i:Int, ?batcher=null){
    super({
      pos: map.HexMap.hex_to_pos(map.HexMap.cubeDir[i]),
      texture: Luxe.resources.texture('res/sprites/ui/hex_button.png'),
      depth: 10,
      scale: new Vec(1,1),
      batcher: batcher == null ? Luxe.renderer.batcher : batcher,
    });
    index = i;

    icon = new Sprite({
      depth: 11,
      color: new luxe.Color().rgb(0x8f563b),
      size: new Vector(20,20),
      batcher: batcher == null ? Luxe.renderer.batcher : batcher,
    });

    icon.parent = this;
    icon.pos = new Vector(10,10);
    InputController.register_click(this, on_click);
    InputController.register_hover(this, function(e:MouseEvent){
      if(UI.actionCost[buttonName] != null){
        Manager.ui.preview_cost(UI.actionCost[buttonName]);
      }else if(C.buildingData[buttonName.split("_")[1]] != null){
        Manager.ui.preview_cost(C.buildingData[buttonName.split("_")[1]].cost);
      }
    }, function(e:MouseEvent){
      Manager.ui.preview_cost_reset();
    });
  }

  public function on_click(e:MouseEvent){
    if(buttonName == 'commit_turn')Luxe.events.fire('commit_turn');
    else click();
  }

  public function click(){
    if(!clickable)return;
    var tag:String = Manager.selectedElement.tag;

    if(C.actions[tag][buttonName] != null){
      Manager.ui.hexButtons.show(tag, buttonName);
    }else if(buttonName == 'back'){
      Manager.ui.hexButtons.back();
    }else{
      if(UI.actionCost[buttonName]!=null)Manager.player.expend(UI.actionCost[buttonName]);
      Luxe.events.fire('infoBar_button', buttonName);
      Manager.ui.hexButtons.validate();
    }
  }

  public function set_clickable(b:Bool){
    clickable = b;
    if(b){
      color = new luxe.Color().rgb(0xFFFFFF);
      icon.color = new luxe.Color().rgb(0x8f563b);
    }else{
      color = new luxe.Color().rgb(0xAAAAAA);
      icon.color = new luxe.Color().rgb(0x663931);
    }
  }
}

class HexHighlight extends Sprite{
  public var gridPos:Vec;
  override public function new(v:Vec, d:Float, anim:Bool){
    super({
      pos: map.HexMap.hex_to_pos(v),
      size: new Vector(32,32),
      texture: Luxe.resources.texture("res/sprites/ui/tile_highlight.png"),
      depth: 5,
    });
    gridPos = v;

    if(anim){
      scale = new Vector(0,0);
      Actuate.tween(scale,.3, {x: 1, y: 1}).delay(d);
    }
  }
}

class InfoText extends Text{
  override public function new(t:String, v:Vec, ?d:Float=0){
    super({
      pos: v,
      depth: 6,
      text: t,
      font: Luxe.resources.font('res/fonts/font.fnt'),
      point_size: 14,
      align: TextAlign.center,
      color: new luxe.Color().rgb(0x8f563b),
    });
    Actuate.tween(pos, 1, {y: pos.y-10}).delay(d).onComplete(function(){
      destroy();
    });

    Actuate.tween(color, .3, {a: 0}).delay(.7+d);
  }
}

class SettlerBar extends Sprite{
  public var value:Int = 0;
  private var bar:Sprite;
  override public function new(ui:Batcher){
    super({
      pos: new Vec(0,Luxe.screen.h/3-13),
      centered: false,
      texture: Luxe.resources.texture('res/sprites/ui/unit_bar.png'),
      batcher: ui,
      depth: 2,
    });

    bar = new Sprite({
      parent: this,
      pos: new Vec(13,7),
      color: new luxe.Color().rgb(0xac3232),
      size: new Vec(0,5),
      centered: false,
      depth: 3,
      batcher: ui,
    });

    new Sprite({
      parent: this,
      pos: new Vec(13,7),
      batcher: ui,
      texture: Luxe.resources.texture('res/sprites/ui/unit_bar_overlay.png'),
      centered: false,
      depth: 4,
    });
  }

  public function value_add(v:Int){
    value += v;
    while(value>20){
      value-=20;
      Manager.game.new_settler();
    }
    bar.size.x = value*5;
  }
}
