package states;

import luxe.Input;
import luxe.Screen;
import luxe.States;
import luxe.Sprite;
import luxe.Vector;
import luxe.tween.Actuate;
import phoenix.Batcher;
import map.*;
import sys.*;
import Manager.Action;

enum GameState{
  SELECTING;
  INTERACTING;
  SELECTING_TILE;
}

enum GameMode{
  SURVIVAL;
  ONLINE;
}

class Game extends State{
  public var players:Array<Player> = [];
  public var map:HexMap;
  public var cursor:HexCursor;
  private var ui:UI;
  private var inDisplay:Bool = false;
  private var ai:Array<AI> = [];
  public var gameState(default, set):GameState = GameState.SELECTING;
  public var gameMode:GameMode;

  override public function onenter<T>(_:T){
    Manager.game = this;
    map = new HexMap(10);
    ui = new UI();
    cursor = new HexCursor();
    cursor.enable();
    players.push(new Player(0, true));
    Manager.player = players[0];
    Manager.ai = new Player(1);

    new InputController();

    ui.hexButtons.clear();
    Luxe.events.listen('commit_turn', commit_turn);
    Luxe.events.listen('start_turn', EnemySquad.control_enemies);

    setup_render();

    Luxe.camera.add(new components.CameraControls({name:'controls'}));
    Luxe.camera.get('controls').focus(Manager.town_centers[0]);
  }

  private function setup_render(){
    Luxe.camera.pos = new Vector(Std.int(-Luxe.screen.width/2),Std.int(-Luxe.screen.height/2));
    Luxe.camera.size = new Vec(Luxe.screen.width/3, Luxe.screen.height/3);
    Luxe.camera.size_mode = luxe.Camera.SizeMode.fit;
  }

  override public function onwindowresized(e:WindowEvent){
    Luxe.camera.size = new Vec(Luxe.screen.width/3, Luxe.screen.height/3);
    ui.resize();
    Luxe.camera.get('controls').focus(Manager.town_centers[0]);
    HexMap.chunks.enable_necessary_chunks();
  }

  public function info_event(cmd:Array<String>){
    Manager.action = Manager.actions[cmd[0]];
    switch(cmd[0]){
      case 'move':
        select_tile();
        // map.display_array(HexMap.range(Manager.selectedElement.gridPos, cast Manager.selectedElement.range, false));
      case "build":
        select_tile();
        cursor.enable('res/sprites/buildings/${cmd[1]}.png');
        if(!C.buildingData[cmd[1]].large) map.display_array(HexMap.adjacent(Manager.selectedElement.gridPos));
        else map.display_array(HexMap.ring(Manager.selectedElement.gridPos, 2));
        Manager.stateInfo = cmd[1];

      case "gather":
        Manager.selectedElement.tile.gather();
        Manager.selectedElement.readiness = 0;
        cast(Manager.selectedElement, Unit).gathering = true;
        gameState = GameState.SELECTING;

      case "attack":
        select_tile();
        map.display_array(HexMap.adjacent(Manager.selectedElement.gridPos));

      case "shoot":
        select_tile();
        var v:Vec = Manager.selectedElement.gridPos;
        var disp:Array<Vec> = HexMap.ring(v, 2).concat(HexMap.ring(v, 3));
        map.display_array(disp);

      case "till":
        select_tile();
        map.display_array(HexMap.range(Manager.selectedElement.gridPos, 2));

      case "plant":
        select_tile();
        map.display_array(HexMap.range(Manager.selectedElement.gridPos, 1));

      case "quarry":
        select_tile();
        map.display_array(HexMap.range(Manager.selectedElement.gridPos, 1));

      case "train":
        Manager.stateInfo = cmd[1]+"_"+cmd[2];
        select_tile();
        map.display_array(HexMap.adjacent(Manager.selectedElement.gridPos));
    }
  }

  public function select_tile(){
    ui.hexButtons.clear();
    gameState = GameState.SELECTING_TILE;
  }

  public function commit_turn(_:Dynamic){
    map.display_clear();
    Manager.selectedElement = null;
    ui.hexButtons.clear();
    gameState = GameState.SELECTING;
    var player:Player = Manager.player;
    for(building in player.buildings){
      if(building.tag == 'house')Turn.add_action('settler_point', {source: player});
    }

    for(unit in player.units){
      if(player.resources['food'] > 0){
        player.expend([0,1,0,0]);
      }else{
        if(Luxe.utils.random.bool(.33)){
          unit.health--;
          FX.starve(unit.gridPos);
        }
      }
    }

    Turn.commit();

    // if(Manager.turns%3==0)EnemySquad.spawn(0);
  }

  public function new_settler(){
    for(building in Manager.player.buildings){
      if(building.tag=='town_center'){
        for(tile in map.adjacent_to(building.tile)){
          if(tile.unit==null){
            map.place_unit('settler', tile.gridPos, Manager.player);
            Turn.add_action("new_settler", {tiles: [tile]});
            return;
          }
        }
      }
    }
  }

  private var selIdx:Int = 0;

  public function l_click(hex:Vec){
    var tile:HexTile = map.get_tile_at(hex);
    if(tile == null)return;
    if(gameState == GameState.INTERACTING){
      if(tile==Manager.selectedElement.tile)selIdx++;
      else selIdx = 0;
    }

    if(gameState == GameState.SELECTING || gameState == GameState.INTERACTING){
      if(tile.elements.length == 0)return;
      var el:HexElement = tile.elements[selIdx%tile.elements.length];
      if(el == Manager.selectedElement)return;
      selected_element(el);
    }

    if(gameState == GameState.SELECTING_TILE){
      Manager.selectedTile = tile;
      selected_tile(tile);
      return;
    }
  }

  public function r_click(hex:Vec){
    if(!ui.hexButtons.back()){
      Manager.selectedElement = null;
      Manager.action = Action.NONE;
      map.display_clear();
      cursor.enable();
      ui.elementDisplay.set_element('');
      gameState = GameState.SELECTING;
    }else{
      map.display_clear();
      cursor.disable();
    }
  }

  private var prevPos:Vec = new Vec(1,1,1);
  override public function onmousemove(e:MouseEvent){
    var pnt:Vector = Luxe.renderer.camera.screen_point_to_world(e.pos);
    var hex:Vec = HexMap.pos_to_hex(pnt);

    if(map.get_tile_at(hex) == null)return;

    switch(gameState){
      case GameState.SELECTING_TILE:
        cursor.set_clickable(map.display_contains(hex));
      case SELECTING:
        cursor.set_clickable(map.get_tile_at(hex).elements.length != 0);
      case GameState.INTERACTING:

    }

    if(Manager.action == null)return;

    switch(Manager.action){
      case Action.MOVE:
        if(HexMap.pos_to_hex(pnt).equals(prevPos))return;
        prevPos = HexMap.pos_to_hex(pnt);
        map.display_clear();

        var path:Array<Vec> = map.find_path(Manager.selectedElement.gridPos, prevPos);

        if(path == null)return;

        if(path.length-1 > cast(Manager.selectedElement, Unit).data.speed)return;
        map.display_array(path, false);
      case Action.BUILD:
        cursor.set_clickable(map.can_build(Manager.stateInfo, hex) && map.display_contains(hex));
      case Action.TRAIN:
        if(Manager.stateInfo=='soldier' || Manager.stateInfo=='archer'){
          if(map.get_tile_at(hex).unit == null){
            cursor.set_clickable(false);
          }else if(map.get_tile_at(hex).unit.tag == 'builder'){
            cursor.set_clickable(true);
          }
        }
      default:
    }
  }

  public function selected_tile(t:HexTile){
    if(Manager.action == Action.MOVE){
      var path:Array<Vec> = map.find_path(Manager.selectedElement.gridPos, t.gridPos);
      if(path != null){
        if(path.length-1 <= cast(Manager.selectedElement, Unit).data.speed){
          Turn.add_action('move', {
            tiles: [map.get_tile_at(path[0]), map.get_tile_at(path[path.length-1])],
            element: Manager.selectedElement,
          });
          map.move_unit(cast Manager.selectedElement, path);
          Manager.selectedElement.readiness = 1;
          ui.hexButtons.show(Manager.selectedElement.tag);
        }
      }
    }

    if(Manager.action == Action.BUILD){
      if(map.can_build(Manager.stateInfo, t.gridPos) && map.display_contains(t.gridPos)){
        if(Manager.player.expend(C.buildingData[Manager.stateInfo].cost)){
          map.place_building(Manager.stateInfo, t.gridPos, Manager.player);
          Turn.add_action('build', {});
        }
        Manager.selectedElement.readiness = 0;
      }
    }

    if(Manager.action == Action.TILL){
      if(t.buildable && map.display_contains(t.gridPos)){
        if(Manager.selectedElement.readiness <= 1){
          t.load_id(5);
          Manager.selectedElement.readiness = 0;
          Turn.add_action('till', {});
        }
      }
    }

    if(Manager.action == Action.PLANT){
      if(t.buildable && map.display_contains(t.gridPos)){
        if(Manager.selectedElement.readiness <= 1){
          t.load_id(13);
          Manager.selectedElement.readiness = 0;
          Turn.add_action('plant', {});
        }
      }
    }

    if(Manager.action == Action.QUARRY){
      if(t.data.name == "Mountains" && map.display_contains(t.gridPos)){
        if(Manager.selectedElement.readiness <= 1){
          t.load_id(12, true, false);
          Manager.selectedElement.readiness = 0;
          Turn.add_action('quarry', {});
        }
      }
    }

    if(Manager.action==Action.TRAIN){
      var cmd:Array<String> = Manager.stateInfo.split("_");
      if(t.unit != null){
        if(t.unit.tag == cmd[0]){
          t.unit.kill();
          map.place_unit(cmd[1], t.gridPos, Manager.player);
        }
      }
    }

    if(Manager.action==Action.ATTACK){
      if(t.unit != null){
        Manager.selectedElement.attack(t.unit);
      }
    }

    if(Manager.action==Action.SHOOT){
      if(t.unit != null){
        Manager.selectedElement.attack(t.unit);
      }
    }


    map.display_clear();
    gameState = GameState.INTERACTING;
    Manager.action = Action.NONE;
    ui.hexButtons.show(Manager.selectedElement.tag);
  }

  public function selected_element(e:HexElement){
    if(gameState == GameState.SELECTING || gameState == GameState.INTERACTING){
      Manager.selectedElement = e;
      ui.elementDisplay.set_element(e.texture.id);

      gameState = GameState.INTERACTING;
      ui.hexButtons.parent = e;
      ui.hexButtons.pos = Vector.Divide(e.size, 2);
      ui.hexButtons.show(Manager.selectedElement.tag);
      map.display_clear();
      // if(Std.is(e, Unit)){
      //   var unit:Unit = cast e;
      //   var range:Array<Vec> = [];
      //   for(v in HexMap.range(unit.gridPos, unit.data.speed)){
      //     var p:Array<Vec> = map.find_path(unit.gridPos, v);
      //     if(p != null){
      //       if(p.length-1 <= unit.data.speed && p[p.length-1].equals(v)){
      //         range.push(v);
      //       }
      //     }
      //   }
      //   map.display_array(range);
      // }
    }
  }

  public function m_wheel(d:Int){
    if(Manager.action == Action.BUILD){
      if(C.buildingData[Manager.stateInfo].flip){
        cursor.flipy = !cursor.flipy;
      }
    }
  }

  //TODO: is there a reason to disable cursor?
  public function set_gameState(s:GameState){
    gameState = s;
    switch(gameState){
      case GameState.SELECTING:
        cursor.enable();
        map.display_clear();
      case GameState.INTERACTING:
        cursor.enable();
        map.display_clear();
      case GameState.SELECTING_TILE:
        cursor.enable();
    }
    return s;
  }
}
