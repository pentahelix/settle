package states;

import luxe.Vector;
import luxe.States;
import luxe.Sprite;
import luxe.tween.Actuate;

class Splash extends State{
  override public function onenter(_){
    Actuate.timer(2.4).onComplete(function(){
       Main.states.set('title');
    });

    new Sprite({
      size: Luxe.screen.size,
      centered: false,
      color: new luxe.Color().rgb(0x222034),
    });

    var logo:Sprite = new Sprite({
      texture: Luxe.resources.texture('res/sprites/misc/therefore_games.png'),
      pos: Luxe.screen.mid,
      scale: new Vec(Math.ceil(Luxe.screen.h/64/2), Math.ceil(Luxe.screen.h/64/2)),
      color: new luxe.Color(1,1,1,0),
    });

    Actuate.timer(.3).onComplete(function(){
      Actuate.tween(logo.color, .3, {a: 1});
    });

    Actuate.timer(2.1).onComplete(function(){
      Actuate.tween(logo.color, .3, {a: 0});
    });
  }

  override public function onleave(_){
    Luxe.renderer.batcher.empty(false);
  }
}
