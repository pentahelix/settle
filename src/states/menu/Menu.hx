package states.menu;

import luxe.States;
import luxe.Sprite;
import luxe.Color;
import luxe.Vector;
import luxe.Input;
import luxe.Text;
import mint.render.luxe.LuxeMintRender;
import mint.focus.Focus;
import mint.layout.margins.Margins;
import mint.types.Types;
import mint.Canvas;
import sys.AutoCanvas;

class Menu extends State{
  public var disp : Text;
  public var canvas: mint.Canvas;
  public var rendering: LuxeMintRender;
  public var layout: Margins;
  public var focus: Focus;

  override public function onenter<T>(_:T){
    rendering = new LuxeMintRender();
    layout = new Margins();
    var auto_canvas = new AutoCanvas({
      name:'canvas',
      rendering: rendering,
      options: { color:new Color(1,1,1,0) },
      // x: -Luxe.screen.w/6, y:-Luxe.screen.h/6, w: Luxe.screen.w/3, h: Luxe.screen.h/3,
      x: 0, y:0, w: Luxe.screen.w, h: Luxe.screen.h,
      scale: 3,
    });
    auto_canvas.auto_listen();
    canvas = auto_canvas;
    focus = new Focus(canvas);
  }

  override function update(dt:Float) {
    canvas.update(dt);
  }

  public function button(x:Int, y:Int, sprite:String, f:luxe.MouseEvent->Void){
    var b:Sprite = new luxe.Sprite({
      pos: new Vec(x*3,y*3),
      scale: new Vec(6,6),
      texture: Luxe.resources.texture("res/sprites/ui/"+sprite),
      depth: 1,
    });

    InputController.register_click(b, f);
  }
}
