package states.menu;

import map.*;
import luxe.Input;
import luxe.Screen;
import luxe.Sprite;
import luxe.States;
import luxe.Vector;
import luxe.Rectangle;
import phoenix.Batcher;

class Title extends Menu{
  var batch:Batcher;
  var rot:Float;
  var map:HexMap;
  var buttons:Sprite;
  var btns:Array<Sprite> = [];
  override public function onenter<T>(_:T){
    super.onenter(_);
    map = new HexMap(20);
    batch = Luxe.renderer.create_batcher({
      name: 'menu',
      layer: 2,
    });
    buttons = new Sprite({
      centered: false,
      pos: new Vec(0,50),
      texture: Luxe.resources.texture('res/sprites/ui/main_menu_buttons.png'),
      batcher: batch,
      depth: 1,
      scale: new Vec(Math.ceil(Luxe.screen.h/122/1.5),Math.ceil(Luxe.screen.h/122/1.5)),
    });
    Luxe.camera.size = Vector.Divide(Luxe.screen.size,3);

    btns.push(new Sprite({
      name: "title_btn_local",
      pos: new Vec(14,54),
      size: new Vec(62,14),
      depth: 3,
      centered: false,
      parent: buttons,
      batcher: batch,
      color: new luxe.Color(0,0,0,0),
    }));
    btns.push(new Sprite({
      name: "title_btn_online",
      pos: new Vec(14,80),
      size: new Vec(62,14),
      depth: 3,
      centered: false,
      parent: buttons,
      batcher: batch,
      color: new luxe.Color(0,0,0,0),
    }));
    btns.push(new Sprite({
      name: "title_btn_settings",
      pos: new Vec(14,106),
      size: new Vec(62,14),
      depth: 3,
      centered: false,
      parent: buttons,
      batcher: batch,
      color: new luxe.Color(0,0,0,0),
    }));
  }

  override public function update(dt:Float){
    rot += dt/5;
    Luxe.camera.pos = new Vec(Math.cos(rot), Math.sin(rot))*150 - Vector.Multiply(Luxe.camera.size, 1.5);
    Luxe.camera.pos = new Vec(Math.round(Luxe.camera.pos.x*3)/3, Math.round(Luxe.camera.pos.y*3)/3);
    HexMap.chunks.enable_necessary_chunks();
  }

  override public function onwindowresized(e:WindowEvent){
    Luxe.camera.size = new Vec(Luxe.screen.width/3, Luxe.screen.height/3);
    batch.view.viewport = new Rectangle(0,0,Luxe.screen.w, Luxe.screen.h);
    HexMap.chunks.enable_necessary_chunks();
    buttons.scale = new Vec(Math.ceil(Luxe.screen.h/122/1.5),Math.ceil(Luxe.screen.h/122/1.5));
  }

  override public function onleave<T>(_:T){
    batch.destroy(true);
    Luxe.renderer.batcher.empty(false);
    btns = [];
  }

  override public function onmousedown(e:MouseEvent){
    for(b in btns){
      if(b.point_inside(e.pos)){
        switch(b.name){
          case 'title_btn_local':
            Main.states.set('game');
            return;
          case 'title_btn_online':

          case 'title_btn_settings':
        }
      }
    }
  }
}
