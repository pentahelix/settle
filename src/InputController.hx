package;

import luxe.Input;
import luxe.Entity;
import luxe.Sprite;
import luxe.Vector;
import map.HexMap;

class InputController extends Entity{
  private static var _clicks:Map<Sprite, MouseEvent->Void> = new Map<Sprite, MouseEvent->Void>();
  private static var _hovers:Map<Sprite, Array<MouseEvent->Void>> = new Map<Sprite, Array<MouseEvent->Void>>();

  public static function register_click(s:Sprite, f:MouseEvent->Void){
    _clicks.set(s, f);
  }

  public static function register_hover(s:Sprite, fIn:MouseEvent->Void, fOut:MouseEvent->Void){
    _hovers.set(s, [fIn, fOut]);
  }

  override public function new(){
    super({name:'InputController'});
    bind_input();
    Luxe.events.listen('infoBar_button', handle_info);
  }

  public function handle_info(name:String){
    var cmd:Array<String> = name.split("_");
    Manager.game.info_event(cmd);
  }

  private function bind_input(){
    Luxe.input.bind_key('camera_up', Key.up);
    Luxe.input.bind_key('camera_down', Key.down);
    Luxe.input.bind_key('camera_left', Key.left);
    Luxe.input.bind_key('camera_right', Key.right);
    Luxe.input.bind_key('pause', Key.escape);
    Luxe.input.bind_key('screenshot', Key.f9);
  }

  override public function onmousedown(e:MouseEvent){
    if(e.button == MouseButton.left){
      var clicked:Array<Sprite> = new Array<Sprite>();
      for(sprite in _clicks.keys()){
        if(sprite.point_inside(sprite.geometry.batchers[0].view.screen_point_to_world(e.pos)))clicked.push(sprite);
      }
      clicked.sort(function(s1:Sprite, s2:Sprite){
        if(s1.depth-s2.depth < 0)return -1;
        if(s1.depth-s2.depth > 0)return 1;
        return 0;
      });
      if(clicked[0]!= null){
        _clicks[clicked[0]](e);
        return;
      }
    }
    var hex:Vec = HexMap.pos_to_hex(Luxe.camera.screen_point_to_world(e.pos));
    switch(e.button){
      case MouseButton.left:
        Manager.game.l_click(hex);
      case MouseButton.right:
        Manager.game.r_click(hex);
      default:

    }
  }

  private var hovered:Sprite;
  override public function onmousemove(e:MouseEvent){
    var hover:Array<Sprite> = new Array<Sprite>();
    for(sprite in _hovers.keys()){
      if(sprite.point_inside(sprite.geometry.batchers[0].view.screen_point_to_world(e.pos)))hover.push(sprite);
    }
    hover.sort(function(s1:Sprite, s2:Sprite){
      if(s1.depth-s2.depth < 0)return -1;
      if(s1.depth-s2.depth > 0)return 1;
      return 0;
    });
    if(hovered == null){
      if(hover[0]!= null){
        _hovers[hover[0]][0](e);
        hovered = hover[0];
      }
    }else{
      if(hover[0] == null){
        _hovers[hovered][1](e);
        hovered = null;
      }else{
        _hovers[hovered][1](e);
        _hovers[hover[0]][0](e);
        hovered = hover[0];
      }
    }
  }

  override public function onmousewheel(e:MouseEvent){
    Manager.game.m_wheel(e.y_rel);
  }

  override public function oninputdown(e:InputEvent){
    switch(e.name){
      case "pause":
        Manager.ui.toggle_pause_menu();
    }
  }
}
