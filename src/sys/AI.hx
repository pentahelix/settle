package sys;
import map.*;

class AI{
  public var resources:Array<Int> = [];
  public var units:Array<UnitAI> = [];
  public var buildings:Array<Building> = [];

  public var mod_gather:Float = 0;
  public var mod_expand:Float = 0;
  public var mod_attack:Float = 0;

  private var pri_gather:Float = 0;
  private var pri_expand:Float = 0;
  private var pri_attack:Float = 0;

  public function new(){
    mod_gather = Luxe.utils.random.get()+.5;
    mod_expand = Luxe.utils.random.get()+.5;
    mod_attack = Luxe.utils.random.get()+.5;
  }

  public function make_turn(){
    pri_gather = mod_gather/1.5;
    pri_expand = mod_expand/1.5;
    pri_attack = mod_attack/1.5;

    if(resources_avg() < 30*mod_gather)pri_gather = 1-(resources_avg()/30*mod_gather);

    if(buildings.length < Manager.turns/2*mod_expand)pri_expand = 1-buildings.length/(Manager.turns/2*mod_expand);

    for(u in units){
      designate_unit(u);
    }

    for(u in units){
      act(u);
    }
  }

  private function designate_unit(u:UnitAI){
    if(u.designated)return;
    var v:Float = Math.max(pri_gather, Math.max(pri_expand, pri_attack));
    if(v == pri_gather){

      pri_gather *= .8;
    }else if(v == pri_expand){

      pri_expand *= .8;
    }else if(v == pri_attack){

      pri_attack *= .8;
    }
    u.designated = true;
  }

  private function act(u:UnitAI){
    switch(u.task.split("_")[0]){
      case "gather":


    }
  }

  private function resources_avg():Float{
    return resources[0]*mod_expand+resources[1]+resources[2]*mod_expand+resources[3]*mod_attack/(mod_expand*2+mod_attack+1);
  }

  private function num_of_units(tag:String):Int{
    return units.filter(function(u:Unit){return u.tag==tag;}).length;
  }

  private function num_of_buildings(tag:String):Int{
    return buildings.filter(function(u:Building){return u.tag==tag;}).length;
  }

  public static var resourcesIds:Map<String, Array<Int>> = [
    "wood" => [],
    "food" => [],
    "rock" => [],
    "iron" => [],
  ];

  private function has_resources(res:Array<Int>){
    if(resources[0]< res[0])return false;
    if(resources[1]< res[1])return false;
    if(resources[2]< res[2])return false;
    if(resources[3]< res[3])return false;
    return true;
  }
}

class UnitAI extends Unit{
  public var designated:Bool = false;
  public var task:String = "";
  public var idle:Int = 0;
  override public function new(name:String){
    super(name);
  }
}
