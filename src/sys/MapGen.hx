package sys;

import luxe.Vector;
import noisehx.Perlin;
import map.HexMap;
import phoenix.Batcher;

using Extensions.VectorExt;
using Extensions.ArrayExt;

typedef Node = {
  var pos:Vec;
  var id:Int;
  var height:Float;
  var water:Bool;
  var coast:Bool;
  var next:Node;
};

class MapGen{
  public static var map:MapData;
  public static function at(pos:Vec){
    return map.at(pos);
  }

  public static function generate(?r:Int=14, ?seed:Int=0){
    map = new MapData(r, seed);
  }


  private static function hex_to_perlin(h:Vec){
    return new Vec((h.x+h.z/2), .8*h.z);
  }
}

class MapData{
  public var r:Int;
  public var nodes:Map<String, Node> = new Map<String, Node>();
  private var noise:Perlin;
  private var biomes:Perlin;

  public function new(radius:Int, seed:Int){
    Manager.town_centers = [];
    r = radius;
    nodes = new Map<String, Node>();
    noise = new Perlin(seed);
    Luxe.utils.random.initial = seed;

    //construction
    for(p in HexMap.range(new Vec(0,0,0), r, true)){
      nodes.set(p.to_string(), {
        id: -1,
        pos: p,
        height: height(p),
        coast: false,
        water: false,
        next: null,
      });
    }

    //initial ids
    for(node in nodes){
      if(node.height < 0.15){
        node.id = 2;
        node.water = true;
      }else{
        node.id = biome(node.pos);
      }
    }

    var coast:Array<Node> = [];

    //coast
    for(node in nodes){
      if(node.water)continue;
      for(v in HexMap.adjacent(node.pos)){
        if(nodes[v.to_string()] == null)continue;
        if(nodes[v.to_string()].water){
          node.coast = true;
          node.id = 9;
          coast.push(node);
          continue;
        }
      }
    }

    //next nodes

    for(node in nodes){
      var next:Node = node;
      for(v in HexMap.adjacent(node.pos)){
        if(nodes[v.to_string()] == null)continue;
        if(nodes[v.to_string()].height > next.height)next = nodes[v.to_string()];
      }
      node.next = next;
    }

    //rivers

    // for(node in coast){
    //   if(Luxe.utils.random.bool(coast.length/20/coast.length)){
    //     var len:Int = Luxe.utils.random.int(4, 7);
    //     var cur:Node = node;
    //     for(i in 0...len){
    //       if(cur.id == 2)break;
    //       cur.id = 2;
    //       cur = cur.next;
    //       if(cur == null)break;
    //     }
    //   }
    // }

    //town center
    var validCenters:Array<Node> = [];
    for(node in nodes){
      var valid:Bool = true;
      if(node.id==0){
        for(h in HexMap.adjacent(node.pos)){
          if(node==null)valid = false;
          else if(nodes[h.to_string()].id!=0 && nodes[h.to_string()].id!=9)valid = false;
        }
        if(valid)validCenters.push(node);
      }
    }

    if(validCenters.length==0){
      trace("no centers");
      Manager.town_centers.push(new Vec(0,0,0));
    }else{
      Manager.town_centers.push(validCenters.random().pos);
    }

    Luxe.utils.random.reset();
  }

  public function at(v:Vec):Int{
    if(nodes[v.to_string()] == null)return -1;
    return nodes[v.to_string()].id;
  }

  public function height(v:Vec){
    return Math.max((perlin(v)+.5), (perlin(v+new Vec(-1000,-1000,-1000))+.5)) * coef(v);
  }

  public function biome(v:Vec){
    var h:Float = height(v);
    var m:Float = (perlin(v*2+new Vec(1000,1000,1000))+.5);
    if(h < .64){
      if(m > .8){
        return 0;
      }else if(m > .1){
        if(Luxe.utils.random.bool(.05))return 3;
        if(Luxe.utils.random.bool(.08))return 3;
        else return 0;
      }else{
        if(m > .03){
          return 4;
        }else{
          if(Luxe.utils.random.bool(.1)){
            if(Luxe.utils.random.bool(.4))return 11;
            else return 10;
          }else{
            return 4;
          }
        }
      }
    }else if(h < .9){
      if(m > .35){
        return 1;
      }else if(m > .3){
        if(Luxe.utils.random.bool(.1)){
          if(Luxe.utils.random.bool(.4))return 11;
          else return 10;
        }else{
          return 1;
        }
      }else{
        return 4;
      }
    }else{
      return 4;
    }
  }

  public function perlin(v:Vec){
    return noise.noise3d(v.x/r, v.y/r, v.z/r);
  }

  public function coef(v:Vec){
    return Math.sqrt((r-dist_center(v))/r);
  }

  private function dist_center(v:Vec){
    return (Math.abs(v.x) + Math.abs(v.y) + Math.abs(v.z))/2;
  }
}
