package sys;

import luxe.Vector;

typedef Action = {
  var name:String;
  var info:ActionInfo;
}

typedef ActionInfo = {
  @:optional var tiles:Array<map.HexTile>;
  @:optional var element:map.HexElement;
  @:optional var value:Int;
  @:optional var source:sys.Player;
}

class Turn{
  private static var actions:Array<Action> = [];

  public static function add_action(n:String, i:ActionInfo){
    actions.push({
      name: n,
      info: i,
    });
    Manager.ui.btnTurn.set_clickable(true);
  }

  public static function commit(){
    Luxe.events.fire('end_turn');
    Manager.turns++;
    for(player in Manager.game.players){
      for(unit in player.units){
        unit.readiness = 2;
      }

      for(building in player.buildings){
        building.readiness = 1;
      }
    }

    for(a in actions){
      if(a.name == "wood" || a.name == "rock" || a.name == "food"){
        Manager.player.resources[a.name] += a.info.value;
        new UI.InfoText(a.info.value+"", Vector.Subtract(map.HexMap.hex_to_pos(a.info.tiles[0].gridPos), new Vector(0,10)));
      }

      if(a.name == "move"){
        new UI.InfoText("+", Vector.Subtract(a.info.element.pos, new Vector(0,10)));
      }

      if(a.name == "settler_point"){
        a.info.source.settler_points++;
      }
    }

    actions = [];
    Luxe.events.fire('start_turn');
    Manager.ui.btnTurn.set_clickable(false);
  }
}
