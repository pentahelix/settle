package sys;
import map.*;
import luxe.Vector;

class Player{
  public var buildings:Array<Building> = [];
  public var units:Array<Unit> = [];

  public var town_center:Vec;

  public var resources:Resources;
  public var settler_points(default, set):Int = 0;

  public var num:Int;

  public function new(n:Int, main:Bool=false){
    num = n;
    if(!main)return;
    resources = new Resources();
    resources['wood'] = 20;
    resources['food'] = 25;
    resources['rock'] = 20;
    resources['iron'] = 5;

    town_center = Manager.town_centers[0];

    Manager.map.place_building('town_center', town_center, this);
    Manager.map.place_unit('settler', town_center+new Vec(-1,1,0), this);
    // Manager.map.place_unit('settler', town_center+new Vec(-1,0,1), this);
    Manager.map.place_unit('settler', town_center+new Vec(0,-1,1), this);
    Manager.map.place_unit('scout', town_center+new Vec(1,-1,0), this);
  }

  public function is_available(arr:Array<Int>){
    if(resources['wood'] < arr[0])return false;
    if(resources['food'] < arr[1])return false;
    if(resources['rock'] < arr[2])return false;
    if(resources['iron'] < arr[3])return false;
    return true;
  }

  public function expend(arr:Array<Int>){
    if(is_available(arr)){
      resources['wood']-=arr[0];
      resources['food']-=arr[1];
      resources['rock']-=arr[2];
      resources['iron']-=arr[3];
      return true;
    }else{
      return false;
    }
  }
  public function set_settler_points(v:Int){
    Manager.ui.settlerBar.value_add(v-settler_points);
    settler_points = v;
    return v;
  }
}

abstract Resources(Map<String,Int>){
  public function new(){
    this = new Map<String, Int>();
    this['wood'] = 0;
    this['food'] = 0;
    this['rock'] = 0;
    this['iron'] = 0;
  }

  @:arrayAccess
  public inline function get(key:String){
    return this.get(key);
  }

  @:arrayAccess
  public inline function arrayWrite(k:String, v:Int):Int{
    this.set(k, v);
    Manager.ui.text[k].text = v+"";
    return v;
  }
}
