package sys;

import map.*;
import luxe.Sprite;
import luxe.Vector;
import luxe.tween.Actuate;
import luxe.components.sprite.SpriteAnimation;

class FX{
  public static function arrow(h1:Vec, h2:Vec){
    var p1:Vec = HexMap.hex_to_pos(h1);
    var p2:Vec = HexMap.hex_to_pos(h2);
    var arrow:Sprite = new Sprite({
      pos: p1,
      texture: Luxe.resources.texture('res/sprites/fx/arrow.png'),
      rotation_z: Math.atan2(p2.y-p1.y,p2.x-p1.x) * 180 / Math.PI,
      depth: 10,
    });
    Actuate.tween(arrow.pos, HexMap.distance(h1,h2)*.2, {x: p2.x, y: p2.y}).onComplete(function(e){
      Actuate.timer(.1).onComplete(function(e){arrow.destroy(true);});
    });
  }

  public static function slash(h1:Vec){
    var slash:Sprite = new Sprite({
      pos: HexMap.hex_to_pos(h1),
      texture: Luxe.resources.texture('res/sprites/fx/slash.png'),
      depth: 10,
      size: new Vec(32,32),
    });
    var anim = slash.add(new SpriteAnimation({name: 'anim'}));
    anim.add_from_json("{\"slash\" : {
        \"frame_size\":{ \"x\":32, \"y\":32 },
        \"frameset\": [\"1-7\"],
        \"loop\": false,
        \"speed\": 21
    }}");
    anim.animation = 'slash';
    anim.play();
    Actuate.timer(.3334).onComplete(function(e){
      slash.destroy();
    });
  }

  public static function starve(h1:Vec){
    var starve:Sprite = new Sprite({
      pos: HexMap.hex_to_pos(h1),
      texture: Luxe.resources.texture('res/sprites/fx/starve.png'),
      depth: 10,
      size: new Vec(32,32),
    });

    var anim = starve.add(new SpriteAnimation({name: 'anim'}));
    anim.add_from_json("{\"starve\" : {
        \"frame_size\":{ \"x\":32, \"y\":32 },
        \"frameset\": [\"1-6\"],
        \"loop\": false,
        \"speed\": 18
    }}");
    anim.animation = 'starve';
    anim.play();
    Actuate.timer(.33334).onComplete(function(e){
      Actuate.tween(starve.pos, .32222, {y: starve.pos.y-16}).onComplete(function(e){
        Actuate.timer(.1).onComplete(function(e){starve.destroy(true);});
      });
    });
  }

  public static function death(h1:Vec){
    var death:Sprite = new Sprite({
      pos: HexMap.hex_to_pos(h1),
      texture: Luxe.resources.texture('res/sprites/fx/death.png'),
      depth: 10,
      size: new Vec(32,32),
    });

    Actuate.tween(death.color, .5, {a: 0});
    Actuate.tween(death.pos, .5, {y: death.pos.y-26}).onComplete(function(e){
      Actuate.timer(.1).onComplete(function(e){death.destroy(true);});
    });
  }
}
