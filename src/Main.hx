package;

import luxe.Vector;
import luxe.States;
import luxe.Sprite;
import luxe.Input;
import phoenix.Texture;
import sys.FileSystem;
import luxe.Parcel;
import luxe.ParcelProgress;

class Main extends luxe.Game{
  public static var states;
  public var renderTex:phoenix.RenderTexture;
  public var renderSize:Vec;
  private  var parcel:Parcel;

  public function init_game(p:Parcel){
    C.init_json();

    //setting up states
    states = new States({name: 'states'});

    states.add(new states.Splash({name: 'splash'}));
    states.add(new states.menu.Title({name: 'title'}));
    states.add(new states.Game({name: 'game'}));

    states.set('splash');
    // states.set('game');
  }

  override public function ready(){
    var shaders = [
      {id:'hex_tile', frag_id:'res/shaders/hex_tile.glsl', vert_id: 'default'},
      {id:'element', frag_id:'res/shaders/element.glsl', vert_id: 'default'},
      {id:'pixelate', frag_id:'res/shaders/pixelate.glsl', vert_id: 'default'},
    ];

    var textures = [];
    for(sprite in get_sprites('res/sprites/')){
      textures.push({id: sprite, filter_mag: FilterType.nearest});
    }

    textures.push({id: 'res/fonts/font.png', filter_mag: FilterType.nearest});

    var jsons = [
      {id: 'res/json/buildings.json'},
      {id: 'res/json/tiles.json'},
      {id: 'res/json/units.json'},
      {id: 'res/json/actions.json'},
    ];

    var fonts = [
      {id:'res/fonts/font.fnt'},
    ];

    parcel = new Parcel({
      shaders: shaders,
      textures: textures,
      jsons: jsons,
      fonts: fonts,
    });

    new ParcelProgress({
      parcel: parcel,
      oncomplete: init_game,
      background: new luxe.Color().rgb(0x222034),
      bar: new luxe.Color().rgb(0xdf7126),
      bar_border: new luxe.Color().rgb(0xdf7126),
      fade_time: .1,
    });

    parcel.load();
  }

  override public function update(dt:Float){
    //
    // if(Luxe.input.keypressed(Key.f5)){
    //   reload_textures();
    // }
  }

  private function batch_pre(b:phoenix.Batcher){
    Luxe.renderer.target = renderTex;
    Luxe.renderer.target_size = renderSize;
  }

  private function batch_post(b:phoenix.Batcher){
    Luxe.renderer.target = null;
    Luxe.renderer.target_size = Luxe.screen.size;
  }

  override public function config(config:luxe.GameConfig):luxe.GameConfig{
    return config;
  }
  private function get_sprites(folder:String):Array<String>{
    var files = [];
    for (file in FileSystem.readDirectory(folder)) {
      if (!FileSystem.isDirectory('${folder}/${file}/')) {
        files.push('${folder}${file}');
      } else {
        for(fl in get_sprites('${folder}${file}/')){
          files.push(fl);
        }
      }
    }
    return files;
  }

  // private function reload_textures(){
  //   for(t in textures){
  //     Luxe.resources.texture(t).reload();
  //     Luxe.resources.load_texture(t, {filter_mag: FilterType.nearest});
  //   }
  // }
}
