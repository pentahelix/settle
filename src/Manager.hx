package;

import states.Game;
import luxe.States;
import map.HexMap;
import map.Unit;
import sys.Player;
import map.Building;
import luxe.Vector;

enum Action{
  NONE;
  MOVE;
  BUILD;
  GATHER;
  ATTACK;
  SHOOT;
  SUICIDE;
  TILL;
  PLANT;
  QUARRY;
  TRAIN;
}

class Manager{
  public static var actions:Map<String, Action> = [
    "move" => Action.MOVE,
    "build" => Action.BUILD,
    "gather" => Action.GATHER,
    "attack" => Action.ATTACK,
    "shoot" => Action.SHOOT,
    "suicide" => Action.SUICIDE,
    "till" => Action.TILL,
    "plant" => Action.PLANT,
    "quarry" => Action.QUARRY,
    "train" => Action.TRAIN,
  ];

  public static var turns:Int = 0;
  public static var player:Player;
  public static var ai:Player;

  public static var game:Game;
  public static var ui:UI;
  public static var map:HexMap;

  public static var selectedElement:map.HexElement;
  public static var selectedTile:map.HexTile;
  public static var stateInfo:String;
  public static var curCat:String;
  public static var action:Action = Action.NONE;

  public static var town_centers:Array<Vec> = [];
}
