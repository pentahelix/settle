package components;

import luxe.Input;
import luxe.Vector;
import luxe.Camera;
import luxe.Component;

class CameraControls extends Component{
  var camera:Camera;
  var modelPos:Vec;
  var moveSpeed:Float = 100;

  override public function init(){
    modelPos = entity.pos.clone();
  }

  override function update(dt:Float){
    var dir:Vec = new Vec(0,0);
    if(Luxe.input.inputdown('camera_up')){
      dir.y-=1;
    }
    if(Luxe.input.inputdown('camera_down')){
      dir.y+=1;
    }
    if(Luxe.input.inputdown('camera_left')){
      dir.x-=1;
    }
    if(Luxe.input.inputdown('camera_right')){
      dir.x+=1;
    }
    if(dir.equals(new Vec(0,0)))return;
    dir = dir.normalized;

    modelPos.x += dir.x*dt*moveSpeed;
    modelPos.y += dir.y*dt*moveSpeed;
    entity.pos.x = Math.round(modelPos.x*3)/3;
    entity.pos.y = Math.round(modelPos.y*3)/3;
    // if(dir.length != 0)
    map.HexMap.chunks.enable_necessary_chunks();
  }

  public function focus(h:Vec){
    entity.pos = map.HexMap.hex_to_pos(h)-Vector.Divide(Luxe.screen.size,2);
    update_model();
    map.HexMap.chunks.enable_necessary_chunks();
  }

  public function update_model(){
    modelPos = entity.pos.clone();
    entity.pos.x = Math.round(modelPos.x);
    entity.pos.y = Math.round(modelPos.y);
  }
}
