package components;

import luxe.Entity;
import luxe.Component;
import luxe.Input;
import snow.api.buffers.Uint8Array;
import phoenix.RenderTexture;
import sys.io.File;
import haxe.io.Bytes;
import haxe.crypto.Crc32;

class CameraScreenshot extends Component{
  private var tex:RenderTexture;
  override public function init(){
    tex = new RenderTexture({
      id: 'screenshot',
      width: Luxe.screen.w,
      height: Luxe.screen.h,
    });
  }

  override function oninputdown(e:InputEvent){
    if(Luxe.input.inputdown('screenshot')){
      var target = Luxe.renderer.target;
      Luxe.renderer.target = tex;
      Luxe.renderer.render_path.render(Luxe.renderer.batchers, Luxe.renderer.stats);
      Luxe.renderer.target = target;
      var w:Int = Luxe.screen.w;
      var h:Int = Luxe.screen.h;
      var data:Uint8Array = new Uint8Array(w*h*4);
      tex.fetch(data);
      var i = 0;

      var time:String = DateTools.format(Date.now(), "%d_%m_%y-%H_%M");
      var file = File.write('E:\\Projects\\settle\\media\\screenshots\\screen_${time}.png',true);

      //.png signature
      var sign:Array<Int> = [137,80,78,71,13,10,26,10];
      var bsign = Bytes.alloc(sign.length);
      for(i in 0...sign.length)bsign.set(i, sign[i]);
      file.write(bsign);

      file.bigEndian = true;
      //IHDR Chunk
      file.writeInt32(13);
      var ihdr:Array<Int> = [
        73,72,68,82,
        w>>24&0xff,w>>16&0xff,w>>8&0xff,w&0xff,
        h>>24&0xff,h>>16&0xff,h>>8&0xff,h&0xff,
        8,6,0,0,0
      ];
      var bihdr = Bytes.alloc(ihdr.length);
      for(i in 0...ihdr.length)bihdr.set(i, ihdr[i]);
      file.write(bihdr);
      file.writeInt32(Crc32.make(bihdr));

      //IDAT Chunk
      var imgf:Bytes = data.toBytes();
      var img:Bytes = Bytes.alloc(imgf.length+h);
      for(i in 0...h){
        img.set((w*4+1)*i, 0);
        img.blit((w*4+1)*i+1, imgf, w*i*4, w*4);
      }
      file.writeInt32(img.length);
      var idat:Array<Int> = [73,68,65,84];
      var bidat = Bytes.alloc(idat.length+img.length);
      for(i in 0...idat.length)bidat.set(i, idat[i]);
      bidat.blit(4,img,0,img.length);
      file.write(bidat);
      file.writeInt32(Crc32.make(bidat));
      //IEND Chunk
      file.writeInt32(0);
      var iend:Array<Int> = [73,69,78,68];
      var biend = Bytes.alloc(iend.length);
      for(i in 0...iend.length)biend.set(i, iend[i]);
      file.write(biend);
      file.writeInt32(Crc32.make(biend));
      trace(Crc32.make(biend));
      file.close();
    }
  }
}
