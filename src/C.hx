package;

import luxe.Color;
import map.Building.BuildingData;
import map.HexTile.TileData;
import map.Unit.UnitData;

class C{
  public static var gameScale:Float = 3;

  //json
  public static var actions:Map<String, Map<String, Array<String>>> = new Map<String, Map<String, Array<String>>>();
  public static var buildingData:Map<String, BuildingData> = new Map<String, BuildingData>();
  public static var unitData:Map<String, UnitData> = new Map<String, UnitData>();
  public static var tileData:Array<TileData> = [];

  public static function init_json(){
    //fuck JSON forever
    var json:Dynamic = Luxe.resources.json('res/json/actions.json').asset.json;
    for(unit in Reflect.fields(json)){
      actions[unit] = new Map<String, Array<String>>();
      var obj = Reflect.field(json, unit);
      for(category in Reflect.fields(obj)){
        var cat:Array<String> = Reflect.field(obj, category);
        actions[unit][category] = cat;
      }
    }


    var buildings = Luxe.resources.json('res/json/buildings.json').asset.json;
    for(building in Reflect.fields(buildings)){
      buildingData[building] = Reflect.field(buildings, building);
    }

    var units = Luxe.resources.json('res/json/units.json').asset.json;
    for(unit in Reflect.fields(units)){
      unitData[unit] = Reflect.field(units, unit);
    }

    var tiles:Array<Dynamic> = Luxe.resources.json('res/json/tiles.json').asset.json;
    for(tile in tiles){
      var c1 = new Color(tile.border[0][0]/255,tile.border[0][1]/255,tile.border[0][2]/255);
      var c2 = new Color(tile.border[1][0]/255,tile.border[1][1]/255,tile.border[1][2]/255);
      tileData.push({
        name: tile.name,
        border: [c1,c2],
        buildable: tile.buildable,
        passable: tile.passable,
        resources: tile.resources,
        variants: tile.variants==null?1:tile.variants,
        permit: tile.permit==null?[]:tile.permit,
      });
    }
  }
}
