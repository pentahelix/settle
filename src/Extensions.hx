package;

import luxe.Vector;
import luxe.Sprite;
import haxe.io.Bytes;
import luxe.Color;

class VectorExt{
  public static function to_string(v:Vector):String{
    return '${v.x},${v.y},${v.z},';
  }

  public static function adjacent_hex(v:Vec):Array<Vec>{
    var result:Array<Vector> = [];
    for(d in map.HexMap.cubeDir){
      result.push(v+d);
    }
    return result;
  }

  public static function nearest_hex(h:Vec){
    var rx:Int = Math.round(h.x);
    var ry:Int = Math.round(h.y);
    var rz:Int = Math.round(h.z);

    var x_diff:Float = Math.abs(rx - h.x);
    var y_diff:Float = Math.abs(ry - h.y);
    var z_diff:Float = Math.abs(rz - h.z);

    if(x_diff > y_diff && x_diff > z_diff){
      rx = -ry-rz;
    }else if(y_diff > z_diff){
      ry = -rx-rz;
    }else{
      rz = -rx-ry;
    }
    h.x = rx;
    h.y = ry;
    h.z = rz;
    return h;
  }

  public static function round(v:Vector){
    v.x = Math.round(v.x);
    v.y = Math.round(v.y);
    v.z = Math.round(v.z);
    return v;
  }
}

class StringExt{
  public static function to_vector(s:String):Vector{
    var val:Array<Float> = [for (str in s.split(",")) Std.parseFloat(str)];
    return new Vector(val[0], val[1], val[2]);
  }
}

class ArrayExt{
  public static function random<T>(a:Array<T>){
    return a[Luxe.utils.random.int(a.length)];
  }
}
